import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10

Window {
    id: nodeRoot
    visible: true
    property int ratio: Screen.pixelDensity
    width: ratio* 80
    height: ratio * 60
    title: qsTr("Pattern Unlock")
    property int diameterOfNode: Math.min(nodeRoot.width/8,nodeRoot.height/8)
    property int radiusOfNode: diameterOfNode / 2

    property var nodeArray: []
    property var patternArray: []

    GridLayout {
        id: gridLayout
        columns: 3
        anchors.fill: parent
        Repeater {
            model: 9
            Node {
                id: theNode
                width: diameterOfNode
                height: width
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                nodeName: index

                Component.onCompleted: {
                    nodeArray.push(this);
                }
            }
        }
    }

    Canvas {
        id: mycanvas
        anchors.fill: parent
        onPaint: {
            var ctx = getContext("2d");
            ctx.clearRect(0,0,mycanvas.width,mycanvas.height);
            if(patternArray.length == 1) {
                ctx.beginPath();
                ctx.moveTo(patternArray[0].x+radiusOfNode, patternArray[0].y+radiusOfNode);
                ctx.lineTo(mouArea.mouseX,mouArea.mouseY);
                ctx.lineWidth = radiusOfNode / 2;
                ctx.lineCap = 'round';
                ctx.strokeStyle = 'blue';
                ctx.stroke();
            }
            else if(patternArray.length > 1) {
                ctx.beginPath();
                ctx.moveTo(mouArea.mouseX,mouArea.mouseY);

                for(var i=patternArray.length-1;i>=0;i--) {
                    ctx.lineTo(patternArray[i].x+radiusOfNode, patternArray[i].y+radiusOfNode);
                    ctx.moveTo(patternArray[i].x+radiusOfNode, patternArray[i].y+radiusOfNode);
                }

                ctx.lineWidth = radiusOfNode / 2;
                ctx.lineCap = 'round';
                ctx.strokeStyle = 'blue';
                ctx.stroke();
            }
        }
    }

    MouseArea {
        id: mouArea
        property bool readyForNextNode: false;

        anchors.fill: parent
        hoverEnabled: true

        onPressed: {
            var node = gridLayout.childAt(mouseX,mouseY);
            if(node) {
                node.isClicked = true
                patternArray.push(node);
                readyForNextNode = false;
                mycanvas.requestPaint();
            }
        }

        onReleased: {
            for(var i=0;i<nodeArray.length;i++) {
                nodeArray[i].isClicked = false
            }

            if(patternArray.length > 0) {
                var password="";
                for(var i2=0;i2<patternArray.length;i2++) {
                    password += patternArray[i2].nodeName;
                }
                console.log("Password is: " + password);
                patternArray = [];
            }

            mycanvas.requestPaint();
        }


        onPositionChanged: {
            var node = gridLayout.childAt(mouseX,mouseY);


            for(var i=0;i<nodeArray.length;i++) {
                nodeArray[i].isHovered = false
            }

            if(node) {
                if(pressed) {
                    node.isClicked = true
                    if(readyForNextNode) {
                        patternArray.push(node);
                        readyForNextNode = false;
                    }
                }
                else {
                    node.isHovered = true;
                }
            }
            else {
                if(pressed) { //holding at an empty space
                    readyForNextNode = true;
                }
            }

            mycanvas.requestPaint();
        }
    }
}

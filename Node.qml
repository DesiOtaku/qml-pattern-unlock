import QtQuick 2.12
import QtGraphicalEffects 1.12


Rectangle {
    id: node

    property bool isClicked: false
    property bool isHovered: false
    property var nodeName

    radius: width
    color: isClicked ? "green" : "black"
    opacity: isHovered | isClicked ? 1 : .5

    Behavior on opacity {
        NumberAnimation {
            duration: 300
        }
    }

    Behavior on color {
        ColorAnimation {
            duration: 300
        }
    }
}
